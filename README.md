# README #

This repository contains the Eclipse project of the language description for the Xtext-based Chapel editor.
There are four other repositories that contain the [UI classes](https://bitbucket.org/ngmschapel/hu.ngms.chapel.ui), the [Eclipse feature](https://bitbucket.org/ngmschapel/hu.ngms.chapel.sdk) and the [unit tests](https://bitbucket.org/ngmschapel/hu.ngms.chapel.tests), the [project wizard](https://bitbucket.org/ngmschapel/hu.ngms.chapel.projectwizard) and the [common files](https://bitbucket.org/ngmschapel/hu.ngms.chapel.common) as well as the [aggregator project](https://bitbucket.org/ngmschapel/hu.ngms.chapel.aggregator).

For more information about the project, see the [wiki](https://bitbucket.org/ngmschapel/hu.ngms.chapel.git/wiki).